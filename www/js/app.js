// Ionic Socket IO app

var app=angular.module('securegram',
  [
    'ionic',
    'ionic.service.core',//'ionic.service.deploy'
    'ionic.service.push',

    'ngCordova',

    'btford.socket-io',
    'angularMoment',
    'ngSanitize',
    'ngStorage',
    'mdo-angular-cryptography',
    'ja.qr'
  ])

  .run(function($ionicPlatform) {
    // Identify app


    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      /*
       if(window.StatusBar) {
       StatusBar.styleDefault();


       document.addEventListener('deviceready',function(){
       try{
       cordova.plugins.notification.local.add({
       id: "1",
       text: data.username + " has sent a message"
       });
       }catch (e){
       alert(JSON.stringify(e.message));
       }
       });
       }*/
      /*  document.addEventListener('deviceready', function () {
       // Android customization
       cordova.plugins.backgroundMode.setDefaults({ text:'Doing heavy tasks.'});
       // Enable background mode
       cordova.plugins.backgroundMode.enable();

       // Called when background mode has been activated
       cordova.plugins.backgroundMode.onactivate = function () {
       setTimeout(function () {
       // Modify the currently displayed notification
       cordova.plugins.backgroundMode.configure({
       text:'Running in background for more than 5s now.'
       });
       }, 5000);
       }
       }, false);*/

    });
  })

  .config(function($stateProvider, $urlRouterProvider) //, $ionicAppProvider)
  {

    /*$ionicAppProvider.identify({
     // The App ID (from apps.ionic.io) for the server
     app_id: '2efafc3e',
     // The public API key all services will use for this app
     api_key: 'e4cbc195d6d0ce0f24d43d9d98469e81d71191ab14a0f995',
     // The GCM project ID (project number) from your Google Developer Console (un-comment if used)
     gcm_id: '585998923550'
     });
     */
    $stateProvider
      .state('chat', {
        url: "/chat/:nickname",
        templateUrl: "templates/chat.html",
        cache: false
      })
      .state('login', {
        url: "/login",
        templateUrl: "templates/login.html"
      })
      .state('settings', {
        url: "/settings",
        templateUrl: "templates/settings.html"
      })
      .state('exit',{
        url: "/exit",
        templateUrl: "templates/exit.html"
      }).
      state('contacts',{
        url: '/contacts',
        templateUrl: "templates/contacts.html"
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');
  })
