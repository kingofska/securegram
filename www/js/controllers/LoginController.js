app.controller('LoginController',
    function($ionicPlatform, $ionicModal, $scope, $state,$sanitize, $cryptoStorage) {
    try{

        $scope.hideBackButton = true;

    var self=this;
    $ionicModal.fromTemplateUrl('my-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });

    self.checkPassword = function(userPassword){
        if(self.mainPassword) {
            if(self.mainPassword === userPassword){
                $state.go('contacts');
            }else{
                if($scope.failedLogins < 3){
                    $scope.failedLogins++;
                    $scope.openModal();
                }else{
                    self.mode = "failed";
                }

            }
        }else {
            $cryptoStorage.mainPassword = userPassword;
            $state.go('contacts');
        }
    };


    $scope.failedLogins = 0;

    var savedNickname = $cryptoStorage.nickName;
    this.nickname = savedNickname;


    self.mainPassword = $cryptoStorage.mainPassword;
    var onDraw = function(){ return false };

    if(self.mainPassword){
        self.mode = "login";
        onDraw = self.checkPassword;
    }else{
        self.mode = "register";
    }

   self.patternLock = new PatternLock("#password",{
        onDraw: onDraw,
        radius: 30,
        margin: 20
    });

    self.closeModal = function(){
        $scope.closeModal();
    };


	self.join=function(){

        var nickname=$sanitize(self.nickname);
        var userPassword = self.patternLock.getPattern();

        if(nickname)
        {
            $cryptoStorage.nickName = nickname;
        }

        if(self.mainPassword) {
            if(self.mainPassword === userPassword){
                $state.go('contacts');
            }else{
                if($scope.failedLogins < 3){
                    $scope.failedLogins++;
                    $scope.openModal();
                }else{
                    self.mode = "failed";
                }

            }
        }else {
            $cryptoStorage.mainPassword = userPassword;
            $state.go('contacts');
        }
	};
    }catch(e){
        alert(JSON.stringify(e.message));
    }

});
