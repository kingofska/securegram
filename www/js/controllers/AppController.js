app.controller('AppController',function(
  $scope,
  $ionicPlatform,
  $ionicUser,
  $ionicPush,
  //$ionicDeploy,
  $rootScope,
  $cryptoStorage
) {
  $ionicPlatform.ready(function() {
    if($cryptoStorage.nickName){
      var user = $ionicUser.get();
      if (!user.user_id) {
        // Set your user_id here, or generate a random one.
        user.user_id = $ionicUser.generateGUID();
      }
      // Add some metadata to your user object.
      angular.extend(user, {
        name: $cryptoStorage.nickName
      });

      // Identify your user with the Ionic User Service
      $ionicUser.identify(user).then(function (data) {
        $scope.identified = true;
        // alert('Identified user ' + user.name + '\n ID ' + user.user_id);
      }).then(function() {
        // alert("starting to register for push");
        $ionicPush.register({
          canShowAlert: true, //Can pushes show an alert on your screen?
          canSetBadge: true, //Can pushes update app icon badges?
          canPlaySound: true, //Can notifications play a sound?
          canRunActionsOnWake: true, //Can run actions outside the app,
          onNotification: function (notification) {
            // Handle new push notifications here
            console.log(notification);
            return true;
          }
        });
      });
    }

    // Handles incoming device tokens
    $rootScope.$on('$cordovaPush:tokenReceived', function(event, data) {
      // alert("Successfully registered token " + data.token);
      console.info('Ionic Push: Got token ', data.token, data.platform);
      $scope.token = data.token;
    });

    /*
    // Update app code with new release from Ionic Deploy
    $scope.doUpdate = function() {
      $ionicDeploy.update().then(function(res) {
        alert('Ionic Deploy: Update Success! ', res);
      }, function(err) {
        alert('Ionic Deploy: Update error! ', err);
      }, function(prog) {
        alert('Ionic Deploy: Progress... ', prog);
      });
    };

    // Check Ionic Deploy for new code
    $scope.checkForUpdates = function() {
      console.log('Ionic Deploy: Checking for updates');
      $ionicDeploy.check().then(function(hasUpdate) {
        alert('Ionic Deploy: Update available: ' + hasUpdate);
        $scope.hasUpdate = hasUpdate;
      }, function(err) {
        alert('Ionic Deploy: Unable to check for updates', err);
      });
    };

    //$scope.checkForUpdates();
    */

  });

  $scope.hideBackButton = true;
});
