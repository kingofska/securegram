app.controller('SettingsController',function($state,$sanitize, $cryptoStorage, $scope) {
  var self=this;


  //sanitize the nickname
  self.nickName = $cryptoStorage.nickName;

  self.save = function(){
    $cryptoStorage.nickName = self.nickName;
    alert("saved");
  };

  //TODO: let a user change his main password
  self.changeMainPassword = function(){

  };

  //TODO: let a user change his nick
  self.changeNickName = function(){

  };

  $scope.$on('$ionicView.afterEnter', function(){
    $('ion-item').removeClass('active');
    $('[href="#/settings"]').parent('ion-item').addClass('active');
  });

});
