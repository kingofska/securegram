var chat=app.controller('ChatController',function(
  $scope,
  $cryptoStorage,
  $stateParams,
  socket,
  $sanitize,$ionicScrollDelegate,$timeout, $cryptoStorage,  $rootScope, $cordovaVibration) {


    $scope.hideBackButton = true;

    var isOnline = function () {
        var isConnected = false;
        var networkConnection = navigator.connection;
        if (!networkConnection.type) {
            $log.error('networkConnection.type is not defined');
            return false;
        }

        switch (networkConnection.type.toLowerCase()) {
            case 'ethernet':
            case 'wifi':
            case 'cell_2g':
            case 'cell_3g':
            case 'cell_4g':
            case '2g':
            case '3g':
            case '4g':
            case 'cell':
            case 'cellular':
                isConnected = true;
                break;
        }

        $log.log('isOnline? '+ isConnected);
        return isConnected;
    };

    var self=this;
  	var typing = false;
  	var lastTypingTime;
  	var TYPING_TIMER_LENGTH = 400;

	 //initializing messages array

    $scope.$on('$ionicView.afterEnter', function(){
        $rootScope.receiverNick= $stateParams.nickname;
        $rootScope.receiverData = $cryptoStorage.contacts[$rootScope.receiverNick];
        $scope.messages= [];

        if($rootScope.receiverData.queuedMessages.length >0){
            $rootScope.receiverData.queuedMessages.forEach(function(data){
                var content  = Aes.Ctr.decrypt(data.message.content, $rootScope.receiverData.secret, 256);
                addMessageToList(data.username,true,content, $rootScope.receiverData.secret);
            });
            $rootScope.receiverData.queuedMessages = [];
        }

    });

    $scope.$on('$ionicView.beforeLeave', function(){
        $rootScope.receiverData = {};
        $rootScope.receiverNick = null;
    });


    socket.removeListener('new message');
    socket.on('new message', function (data) {
        if (data.message && data.username) {
            if (data.username != $rootScope.receiverData.nickname) {
                if ($cryptoStorage.contacts[data.username]) {
                    $cryptoStorage.contacts[data.username].queuedMessages.push(data);
                    try{
                        localNotification.add("W1" + new Date(),{
                            title: "new message",
                            message: data.username + " has sent a message",
                            badge: 1
                        });
                    }catch (e){
                        alert(JSON.stringify(e.message));
                    }

                } else {
                  //TODO: Handle unknown user
                    alert("unknown user messaging you");
                }

            } else {
                var content = Aes.Ctr.decrypt(data.message.content, $rootScope.receiverData.secret, 256);
                addMessageToList(data.username, true, content, $rootScope.receiverData.secret);
            }

            try {
              $cordovaVibration.vibrate(100);
            } catch (e) {
              //alert(e);
            }
        }
    });


  	socket.on('connect',function(){

        connected = true;

        var selfNick = $cryptoStorage.nickName;





	  // Whenever the server emits 'user joined', log it in the chat body
	  socket.on('user joined', function (data) {
	  	addMessageToList("",false,data.username + " joined");
	  	//addMessageToList("",false,message_string(data.numUsers))
	  });

	  // Whenever the server emits 'user left', log it in the chat body
	  socket.on('user left', function (data) {
	    addMessageToList("",false,data.username+" left");
	   // addMessageToList("",false,message_string(data.numUsers))
	  });

	  //Whenever the server emits 'typing', show the typing message
	  socket.on('typing', function (data) {
	    addChatTyping(data);
	  });

	  // Whenever the server emits 'stop typing', kill the typing message
	  socket.on('stop typing', function (data) {
	    removeChatTyping(data.username);
	  });
    }.bind($scope));

  	//function called when user hits the send button
  	self.sendMessage=function(){
        var content = Aes.Ctr.encrypt(self.message, $rootScope.receiverData.secret, 256);
  		socket.emit('new message', {
            content: content,
            to: $stateParams.nickname
        });
  		addMessageToList($cryptoStorage.nickName, true, self.message, $rootScope.receiverData.secret, true);
  		socket.emit('stop typing');
  		self.message = ""
  	};

  	//function called on Input Change
  	/*self.updateTyping=function(){
  		sendUpdateTyping()
  	};
    */
  	// Display message by adding it to the message list
  	function addMessageToList(username,style_type,message, secretKey){
        try{
            $cordovaVibration.vibrate(100);

        }catch (e){
            //alert(e);
        }

        username = $sanitize(username);
  		removeChatTyping(username);
        var content = $sanitize(message);

  		$scope.messages.push({
            content: content,
            style:style_type,
            username:username,
            color: $rootScope.receiverData.color,
            own: $cryptoStorage.nickName === username,
            date: Date.now()
        });

  		try{
            $ionicScrollDelegate.scrollBottom();
        }catch(e){

        }
  	}

  	// Updates the typing event
  /*	function sendUpdateTyping(){
  		if(connected){
  			if (!typing) {
		        typing = true;
		        socket.emit('typing');
		    }
  		}
  		lastTypingTime = (new Date()).getTime();
  		$timeout(function () {
	        var typingTimer = (new Date()).getTime();
	        var timeDiff = typingTimer - lastTypingTime;
	        if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
	          socket.emit('stop typing');
	          typing = false;
	        }
      	}, TYPING_TIMER_LENGTH)
  	}
*/
	// Adds the visual chat typing message
	function addChatTyping (data) {
	    addMessageToList(data.username,true," is typing");
	}

	// Removes the visual chat typing message
	function removeChatTyping (username) {
	  	$scope.messages = $scope.messages.filter(function(element){return element.username != username || element.content != " is typing"})
	}

});

