app.controller('ContactController',function(
  $scope, $state,$sanitize,
  $cryptoStorage,
  $ionicModal, $ionicActionSheet) {
  $scope.$on('$ionicView.afterEnter', function(){
    $('ion-item').removeClass('active');
    $('[href="#/contacts"]').parent('ion-item').addClass('active');

  });

  //Add colors
  var COLORS = [
    '#e21400', '#91580f', '#f8a700', '#f78b00',
    '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
    '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
  ];

  //Generate color for the same user.
  function getUsernameColor (username) {
    // Compute hash code
    var hash = 7;
    for (var i = 0; i < username.length; i++) {
      hash = username.charCodeAt(i) + (hash << 5) - hash;
    }
    // Calculate color
    var index = Math.abs(hash % COLORS.length);
    return COLORS[index];
  }

  /**
   *
   * @param length
   * @returns {string}
   */
  function generateHexString(length) {
    var ret = "";
    while (ret.length < length) {
      ret += Math.random().toString(16).substring(2);
    }
    return ret.substring(0,length);
  }

  $scope.hideBackButton = true;
  $scope.leftButtons = [{
    type: 'button-icon icon ion-navicon',
    tap: function(e) {
      $scope.sideMenuController.toggleLeft();
    }
  }];

  var self=this;



  if(!$cryptoStorage.contacts){
    $cryptoStorage.contacts = {};
  }
  self.contacts = $cryptoStorage.contacts;

  self.mode = "Add contact";

  $ionicModal.fromTemplateUrl('add-contact-form-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.addContactFormModal = modal;
  });

  self.showAddContactMenu = function(){
    // Show the action sheet
    var hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: 'Generate QR code' },
        { text: 'Add contact from QR code' },
        { text: 'Add contact manually' }
      ],
      titleText: 'Add new contact',
      cancelText: 'Cancel',
      buttonClicked: function(index) {
        var actions = {
          0 : "showQrForm",
          1 : "addFromPairingQr",
          2 : "showManualAdd"
        };
        self[actions[index]]();
        return true;
      }
    });
  };

  self.addNewContact = function(){
    $cryptoStorage.contacts[self.nickNameToAdd] = {
      nickname: self.nickNameToAdd,
      secret: self.secret,
      color: getUsernameColor(self.nickNameToAdd),
      dateAdded: +new Date(),
      queuedMessages: []
    };
    $scope.addContactFormModal.hide();
  };

  $scope.isEmpty = function (obj) {
    return angular.equals({},obj);
  };

  self.showManualAdd = function(){
    self.mode = "Manual add";
    $scope.addContactFormModal.show();
  };

  self.showQrForm = function(){
    $scope.addContactFormModal.show();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

self.edit=function(contact){
  self.mode = "Edit Contact";
  self.nickname = contact.nickname;
  self.secret = contact.secret;
};

self.remove = function(contact){
  delete(self.contacts[contact]);
};

self.addFromPairingQr = function(){
  document.addEventListener("deviceready", function () {

    cordova.plugins.barcodeScanner.scan(
      function (result) {
        /*alert("We got a barcode\n" +
         "Result: " + result.text + "\n" +
         "Format: " + result.format + "\n" +
         "Cancelled: " + result.cancelled);
         */
        var data = JSON.parse(result.text);

        $cryptoStorage.contacts[data.nickName] = {
          nickname: data.nickName,
          secret: data.secret,
          color: getUsernameColor(data.nickName),
          dateAdded: +new Date(),
          queuedMessages: []
        };
        $scope.$apply();
      },
      function (error) {
        alert("Scanning failed: " + error);
      }
    );
  });
};

self.showPairingQr =function(){
  var secret = generateHexString(128);
  var data = JSON.stringify({ nickName: $cryptoStorage.nickName, secret: secret});
  $cryptoStorage.contacts[self.nickNameToAdd] = {
    nickname: self.nickNameToAdd,
    secret: secret,
    color: getUsernameColor(self.nickNameToAdd),
    dateAdded: + new Date(),
    queuedMessages : []
  };
  $scope.qrData = data;
  $scope.addContactFormModal.hide();
  $ionicModal.fromTemplateUrl('view-pairing-qr-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
    $scope.modal.show();
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });


};

});


app.controller('ContactsMenuController',function($scope, $state,$sanitize, $cryptoStorage, $rootScope)//, $ionicPlatform, $ionicUser, $ionicPush)
{

  if (!$cryptoStorage.contacts) {
    $cryptoStorage.contacts = {};
  }

  $scope.storage = $cryptoStorage;
  $scope.contacts = $cryptoStorage.contacts;
  $rootScope.newMessages = 0;
  $scope.$watch("contacts", function(){
    $rootScope.newMessages = 0;
    for(var contact in $scope.storage.contacts){
      $rootScope.newMessages += $scope.storage.contacts[contact].queuedMessages.length;
    }
  },true);



});
