app.factory('socket',function(socketFactory, $cryptoStorage, $ionicPopup){
  //Create socket and connect to http://chat.socket.io
  var myIoSocket = io.connect('http://securapp.herokuapp.com');
  //var myIoSocket = io.connect('http://localhost:3000');

  mySocket = socketFactory({
    ioSocket: myIoSocket
  });

  var savedNickname = $cryptoStorage.nickName;
  var userToken = $cryptoStorage.userToken;

  mySocket.on('connect',function(){
    mySocket.emit('addUser', {
      nickname: savedNickname,
      token: userToken
    });

    mySocket.on('login', function (data) {
      //socket.connected = true;
      if(data.result !== 200){
        var messages = {
          401 : "Wrong token, try again or change username..",
          409 : "Username already taken"
        };
        $ionicPopup.alert({
          title: 'Authentication Error',
          template: messages[data.result],
          okType: "button-balanced"
        });
      }else{
        $cryptoStorage.userToken = data.token;
        //$cryptoStorage.$apply();
      }

    });
  });


  return mySocket;
});
