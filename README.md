#Securegram

##Intro
This aims to be a mobile messaging service with security and privacy in mind.
No data is stored on the backend, all the communication is encrypted on the client with 128bit RSA by JS,
client-persisted data is encrypted with 128 bit RSA ( todo !! )

##Features
- Main app protection with pattern-lock
- Auto generated 256 bit key to be used for AES encryption
- QR code to automatically add other contact with name / key
- Localstorage to persist data ( TODO: encrypt it )

##Todo
- Integrate push notification
  1. create a backend which let user register/login, and stores their device token
  2. add "ring functionality": socket.io(ring) => server name-to-device-token => Push notification => device
- fix localnotification
- fix "user is typing"
- better key generation ?
  currently AES 128bit random hex string
- fix unknown user message

##Install

    npm install
    bower install
    ionic add ionic-service-core
    ionic add ngCordova
    ionic add ionic-service-push
    ionic plugin add https://github.com/phonegap-build/PushPlugin.git
    ionic config set dev_push (true|false)
  

1.  bower install
2.  ionic config set gcm_key <project_id>
3. ionic push --google-api-key <api_key>